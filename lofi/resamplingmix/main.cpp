#include <iostream>
#include <string>
#include "AudioFile-1.0.9/AudioFile.h"

using std::string;

void usage(){
	std::cout << "Usage: resamplemix [file.wav]\n";
}

int main(int argc, char *argv[]){
	if (argc<2){
		usage();
		return 1;
	}

	// Load file
	AudioFile <double> file;
	file.load(argv[1]);

	const int nChannels  = file.getNumChannels();
	const int nSamples   = file.getNumSamplesPerChannel();
	const int bitDepth   = file.getBitDepth();
	const int sampleRate = file.getSampleRate();

	// Create output buffer
	AudioFile <double>::AudioBuffer outputBuffer;
	outputBuffer.resize(nChannels);
	for (int channel = 0; channel < nChannels; channel++)
		outputBuffer[channel].resize(nSamples);

	AudioFile <double> outputFile;

	int resampleRate = 10000;
	double mix = 1;

	double sample;
	double timeSecs;
	for (int channel = 0; channel < nChannels; channel++){
		for (int i = 0; i < nSamples; i++){
			timeSecs += 1 / double(sampleRate);

			if (timeSecs > (1 / double(resampleRate))){
				timeSecs -= (1 / double(resampleRate));
				sample = file.samples[channel][i];
			}

			outputBuffer[channel][i] = sample * mix + sample * (1-mix);
		}
	}

	outputFile.setAudioBuffer(outputBuffer);
	outputFile.setAudioBufferSize(nChannels, nSamples);
	outputFile.setNumChannels(nChannels);
	outputFile.setBitDepth(bitDepth);
	outputFile.setSampleRate(sampleRate);

	outputFile.save("RESAMPLEMIXED_" + string(argv[1]));
}
