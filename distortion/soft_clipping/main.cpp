#include <iostream>
#include <math.h>
#include <string>
#include "AudioFile-1.0.9/AudioFile.h"

#define M_PI 3.14159265358979323846

using std::string;

void usage(){
	std::cout << "Usage: softclipper [file.wav]\n";
}

int main(int argc, char *argv[]){
	if (argc<2){
		usage();
		return 1;
	}

	// Load file
	AudioFile <double> file;
	file.load(argv[1]);

	const int nChannels  = file.getNumChannels();
	const int nSamples   = file.getNumSamplesPerChannel();
	const int bitDepth   = file.getBitDepth();
	const int sampleRate = file.getSampleRate();

	// Create output buffer
	AudioFile <double>::AudioBuffer outputBuffer;
	outputBuffer.resize(nChannels);
	for (int channel = 0; channel < nChannels; channel++)
		outputBuffer[channel].resize(nSamples);

	AudioFile <double> outputFile;

	double clippingAmount = 9.5;
	bool dualClip = true;

	for (int channel = 0; channel < nChannels; channel++){
		for (int i = 0; i < nSamples; i++){
			double sample = file.samples[channel][i];

			outputBuffer[channel][i] = (2/M_PI) * atan(clippingAmount * sample);
		}
	}

	outputFile.setAudioBuffer(outputBuffer);
	outputFile.setAudioBufferSize(nChannels, nSamples);
	outputFile.setNumChannels(nChannels);
	outputFile.setBitDepth(bitDepth);
	outputFile.setSampleRate(sampleRate);

	outputFile.save("SOFTCLIPPED_" + string(argv[1]));
}
