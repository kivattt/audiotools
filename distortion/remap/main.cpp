#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include "AudioFile-1.0.9/AudioFile.h"

using std::string;
using std::vector;

void usage(){
	std::cout << "Usage: remapdistortion [file.wav]\n";
}

int main(int argc, char *argv[]){
	if (argc<2){
		usage();
		return 1;
	}

	// Load file
	AudioFile <double> file;
	file.load(argv[1]);

	const int nChannels  = file.getNumChannels();
	const int nSamples   = file.getNumSamplesPerChannel();
	const int bitDepth   = file.getBitDepth();
	const int sampleRate = file.getSampleRate();

	const int sampleHighest = 1 << bitDepth;

	std::cout << "Creating random remap table...\n";
	srand(time(nullptr));

	// Index is in long long, 0 to 2^bitDepth, values are double -1.0 to 1.0
	vector <double> remapTable(1 << bitDepth);

	const unsigned long long nSamplesRefreshBlur = 8000;
	const double mix = 0.08;

	double randomToReach;
	double lastReached = 0;
	double jeff;
	for (unsigned long long i=0; i < sampleHighest; i++){
		if (i % nSamplesRefreshBlur == 0)
			randomToReach = (rand()/double(RAND_MAX)) * 2 - 1;
			jeff = 0;

		jeff += randomToReach / nSamplesRefreshBlur;
		remapTable[i] = lastReached + jeff;

		lastReached = randomToReach;
	}

	// Create output buffer
	AudioFile <double>::AudioBuffer outputBuffer;
	outputBuffer.resize(nChannels);
	for (int channel = 0; channel < nChannels; channel++)
		outputBuffer[channel].resize(nSamples);

	AudioFile <double> outputFile;

	for (int channel = 0; channel < nChannels; channel++){
		for (int i = 0; i < nSamples; i++){
			double sample = file.samples[channel][i];

			unsigned long long sampleInt = (sample + 1) / 2 * sampleHighest;

			outputBuffer[channel][i] = remapTable[sampleInt] * mix + sample * (1-mix);
		}
	}

	outputFile.setAudioBuffer(outputBuffer);
	outputFile.setAudioBufferSize(nChannels, nSamples);
	outputFile.setNumChannels(nChannels);
	outputFile.setBitDepth(bitDepth);
	outputFile.setSampleRate(sampleRate);

	outputFile.save("REMAPPED_" + string(argv[1]));
}
