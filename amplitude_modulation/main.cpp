// Assumes both files have same number of channels, same bitDepth, same sampleRate

#include <iostream>
#include <string>
#include <math.h>
#include "AudioFile-1.0.9/AudioFile.h"

using std::string;

void usage(){
	std::cout << "Usage: amplitudemod [file.wav] [controllerfile.wav]\n";
}

int main(int argc, char *argv[]){
	if (argc<2){
		usage();
		return 1;
	}

	// Load file
	AudioFile <double> file;
	file.load(argv[1]);

	AudioFile <double> controllerFile;
	controllerFile.load(argv[2]);

	const int nChannels  = file.getNumChannels();
	const int nSamples   = file.getNumSamplesPerChannel();
	const int bitDepth   = file.getBitDepth();
	const int sampleRate = file.getSampleRate();

	// Create output buffer
	AudioFile <double>::AudioBuffer outputBuffer;
	outputBuffer.resize(nChannels);
	for (int channel = 0; channel < nChannels; channel++)
		outputBuffer[channel].resize(nSamples);

	AudioFile <double> outputFile;

	for (int channel = 0; channel < nChannels; channel++){
		for (int i = 0; i < nSamples; i++){
			double sample = file.samples[channel][i];
			double controllerSample = controllerFile.samples[channel][i];

			outputBuffer[channel][i] = sample * controllerSample;
		}
	}

	outputFile.setAudioBuffer(outputBuffer);
	outputFile.setAudioBufferSize(nChannels, nSamples);
	outputFile.setNumChannels(nChannels);
	outputFile.setBitDepth(bitDepth);
	outputFile.setSampleRate(sampleRate);

	outputFile.save("AMPLITUDEMOD_" + string(argv[1]));
}
